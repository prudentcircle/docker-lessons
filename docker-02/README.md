# 02. Docker 실습 두 번째 시간 - 기본 명령어
## 1. 인사말
Docker 실습 두 번째 시간입니다. 이번에는 Docker를 사용할때 필수적으로 사용하게 되는 기본 명령어들에 대해서 알아보겠습니다.

## 2. Vagrant 와 Git
모든 실습은 Vagrant을 통해 생성된 VM 에서 진행됩니다. Vagrant의 사용법에 대해서는 첫번째 실습 시간의 설명 문서를 참고해 주세요. GitLab에 올려져 있는 실습자료를 받는 방법에 대해서도 첫번째 실습 시간의 문서를 참고해 주세요.

## 3. 실습내용
실습을 위해서는 vagrant up 을 실행하여 VM을 실행시킨 후 `vagrant ssh docker02` 라는 명령어로 VM에 접근합니다.

### 3.1 컨테이너 목록 확인하기
이미 nginx 컨테이너가 한 개 떠 있습니다. `docker container ls ` 명령어로, 현재 동작하고 있는 컨테이너에 대한 정보를 확인합니다.

### 3.2 html 내용 확인하기
VM이 아닌, 여러분 PC의 웹 브라우저에서 `http://localhost:6789` 로 접속해 ‘Welcome to Nginx’ 환영 메시지를 확인합니다. 

### 3.3 컨테이너 안으로 접속해 들어가기
VM 환경과 다르게, 컨테이너에 접근 할 때에는 SSH를 사용하지 않습니다. 이미 동작하고 있는 컨테이너에 접근하려면 여러분의 VM으로 다시 돌아가서 아래 명령어를 사용합니다.  
`docker container exec -it nginx /bin/bash`

위 명령어는 아래 명령어의 줄임 형태 입니다. Docker 명령어의 모든 옵션값에는  줄임말 형태가 있고, 풀네임의 형태가 있습니다. 풀 네임은 dash(-)가 두 개, 줄임형태는 dash(-)가 한개 입니다.   
`docker container exec --interactive --tty nginx /bin/bash`

성공적으로 명령어가 동작했다면, 명령어 창이 뭔가가 바뀐것이 보이실겁니다.

### 3.4 내용 변경하기
우리는 이제 웹페이지 내용을 변경해 볼 것입니다.
내용을 변경하려면 vim 라는 프로그램이 필요합니다. 아래 명령어로 먼저 설치해 줍니다.
`apt-get update`   
`apt-get install vim`   

이제 우리가 수정을 할 내용이 있는 디렉토리로 이동합니다.   
`cd /usr/share/nginx/html`   
수정을 합시다.   
`vim index.html`   

“Welcome to Nginx”를 “Hello My Docker World!!” 로 바꿔줍니다. 저장을 하고 컨테이너에서 빠져나옵니다.   
`exit`

### 3.5 html 다시 확인하기
우리가 변경한 내용이 반영 되었는지를 확인해 봅니다.   
`http://localhost:6789`   
우리가 수정한대로 내용이 변경 되어있죠?

### 3.6. nginx 컨테이너 삭제하기
우리가 생성했던 container를 삭제합니다. 삭제를 하려면 먼저 컨테이너를 중지시켜야 합니다.   
`docker container stop nginx`   
잘 중지가 되었는지 확인을 해 봅니다.   
`docker container ls`   

근데 왠일인지 컨테이너가 보이지 않죠? 위 명령어는 현재 동작하고 있는 컨테이너만 보여줍니다. 정지되어있는 컨테이너 까지 확인하려면 `--all` 옵션값을 사용해야 합니다.   
`docker container ls --all`   

정지되어있는 컨테이너를 확인합니다. 그리고 삭제를 해 줍니다.   
`docker container rm nginx`   

### 3.7 nginx 컨테이너 다시 생성하기
nginx 컨테이너를 다시 생성해봅니다.   
`docker container run --name nginx nginx:latest`

명령어가 입력이 안되는 상태가 된 것을 확인하세요.  이 상태에서 바로 접속을 해 봅니다.   
`http://localhost:6789`   
접속이 안 될 겁니다. 왜냐하면 명령어에서 설정을 해 주어야만 하거든요.
일단 container 를 삭제합니다.   
`docker container rm nginx`   

### 3.8 Publish 옵션값 사용하기
방금 컨테이너를 실행할때는 포트에 접근을 가능하게 해주는 옵션값이 빠져있었습니다. 이번에는 `--publish` 옵션값을 사용하여 다시 container를 실행합니다.   
`docker container run --publish 80:80 nginx:latest `

### 3.9 내용 확인하기
`http://localhost:6789` 로 확인을 해 보면 값이 다시 초기상태로 돌아온것을 확인할 수 있습니다. 컨테이너에서 변경된 내용은 컨테이너가 사라지면 같이 사라져버립니다.

### 3.10 컨테이너 강제 삭제하기
실행되고 있는 컨테이너를 강제로 삭제 해 보겠습니다.
`docker container rm --force nginx` 를 입력하면 동작하고 있는 컨테이너가 바로 삭제가 됩니다.

### 3.11 컨테이너를 실행시키면서 특정 명령어를 실행하기
nginx 컨테이너를 다시 한번 실행시킵니다. 하지만, 이번에는 컨테이너가 실행되면서 어떤 명령가 실행되도록 해 보겠습니다.     
`docker container run --publish 80:80 --name nginx nginx:latest ping 8.8.8.8`   
8.8.8.8로 ping이 실행되는것을 확인할 수 있습니다. 

### 3.12 확인해보기
`http://localhost:6789` 로 홈페이지가 접속이 되나요?
안되는데에는 다 이유가 있습니다.  기본적으로 nginx 컨테이너는 실행될때에 웹페이지를 띄우는 명령어를 실행하도록 되어있습니다. 

하지만 우리는 그 명령어 대신에 ping 를 실행하라고 했기 때문에 실행이 되지 않는것입니다.

### 3.13 컨테이너 삭제하기
다시한번만 컨테이너를 삭제합니다.
`docker container rm -f nginx`
매번 컨테이너를 삭제하기가 번거로우시지 않은가요? 

### 3.14 마지막으로 컨테이너 생성

우리는 마지막으로 컨테이너를 또 실행시킬껀데, 이번에는 정지되면 알아서 삭제되도록 생성할 것입니다.  이때에는 `--rm` 옵션을 줍니다.

그리고 이번에는 Background에서 실행되도록 `--detach` 옵션을 줄 것입니다. 
백그라운드에서 실행시킨다는 뜻은, container를 실행하더라도 티가 나지 않게 뒷 단에서 돌아간다는 뜻입니다.  

`docker container run --detach --publish 80:80 --rm --name nginx nginx:latest`

### 3.15 컨테이너 정지시키기
컨테이너를 정지시키고, 정말 자동으로 삭제되는지를 확인합니다.
`docker container stop nginx`
`docker container ls --all`
목록을 출력해봐도 아무 컨테이너도 없는것을 확인 하실 수 있습니다.

## 4. 정리
꽤나 많은 명령어를 배웠죠? 오늘 배운 명령어를 정리를 해 보는 시간을 가져봅시다.
-  `docker container ls`  
- `docker container ls --all`  
- `docker container exec --interactive --tty <컨테이너 이름> <명령어>`
- `docker container stop  <컨테이너 이름>`
- `docker container rm <컨테이너 이름>`
- `docker container rm --force <컨테이너 이름>`
- `docker container run --publish <호스트 포트>:<컨테이너 포트> --name <컨테이너 이름> <이미지 이름:태그>`
- `docker container run --rm --name <컨테이너 이름> <이미지 이름:태그>`
- `docker container run --detach --name <컨테이너 이름> <이미지 이름:태그>`
- `docker container run -it --name <컨테이너 이름> <이미지 이름:태그> <명령어>`