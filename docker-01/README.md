# 01. Docker 실습 첫 번째 시간
## 1. 인사말
안녕하세요. Docker 실습의 첫번째 시간입니다. 이번 시간에는 Docker를 설치하고 컨테이너를 실행해 보면서 뭔지 모르게 친숙해지는 시간을 가지는 시간입니다.

## 2. Vagrant
모든 실습은 Vagrant를 통해 생성된 VM 내에서 진행됩니다. 만약 Vagrant 사용법을 모르신다면, 아래 3개 명령어만 기억하시면 됩니다!  
`vagrant status` - 어떤 VM이 생성되는지 확인 / VM 이름 확인   
`vagrant up ` - VM 생성 및 시작   
`vagrant ssh <VM 이름>` - 해당 VM으로 ssh 접근하기   

## 3. Git 사용
제 Repo를 Clone 하시고, “01.Introduction” 디렉토리 내에서 Vagrant를 실행하셔야 합니다.  `git clone` 하는 방법을 모르신다면 관련 내용을 네이버에서 찾아보시기 바랍니다

## 4. 실습내용
앞서 말씀드렸듯이 실습을 위해서는 `vagrant up` 을 실행하여 VM을 실행시키시고, `vagrant ssh docker01` 이라는 명령어로 VM에 접근하시면 됩니다.  모든 실습내용은 docker01 이라는 이름의 VM 안에서 실행합니다.

### 4.1 Docker 설치
https://get.docker.com 에 접속하면 알 수 있는 내용입니다. 굳이 설명하자면, 두 가지 방법중 한가지를 고르시면 됩니다.

첫번째 방법은 wget을 이용한 설치 입니다. wget이 설치가 되어있어야 합니다.   
`wget -qO- https://get.docker.com/ | sh`

두번째 방법은  curl을 이용한 설치입니다. wget을 설치하기도 귀찮으시다면 이걸 사용하시면 됩니다.    
`curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh`

### 4.2 Docker 접근 설정

설치가 끝나면 `sudo usermod -aG docker <현재 user>` 명령어를 실행시켜 줍니다. 이 명령어는 현재 사용자에게 docker에 대한 접근 권한을 줍니다.

해당 명령어를 입력하신 뒤 `exit`을 실행하여 VM에서 빠져나가야 합니다. `vagrant ssh docker01` 명령어를 통해 다시 VM에 접속하셔야만 Docker에 원활하게 접근 할 수 있습니다.

### 4.3 Docker Engine 실행
Docker를 설치만 했지 아직 실행은 하지 않았습니다. 이제 실행시켜줍니다.   
`sudo systemctl start docker`   
`sudo systemctl enable docker`   

### 4.4 Docker 첫 실행
nginx 컨테이너를 실행해 봅시다.   
`docker container run --publish 80:80 nginx`

아마 명령어 창이 아래 메시지에서 멈춰 있을겁니다. 그게 정상입니다.   
`Status: Downloaded newer image for nginx:latest`   

### 4.5 nginx 컨테이너 확인
우리는 방금 nginx 컨테이너를 실행시켰습니다.

VM이 아닌, 여러분의 PC의 인터넷 브라우저 창에서 `http://localhost:6789`에 접속하여 nginx 컨테이너에 접속이 잘 되는지를 확인합니다.

“Welcome to nginx!” 라는 환영문구가 잘 보이시나요?  축하드립니다. 여러분의 첫 성공적인 컨테이너 실행입니다.

### 4.6 제대로 된 nginx 컨테이너 띄워보기
뭔가 조금 아쉽죠. 저희는 3개의 컨테이너를 더 실행해 볼 겁니다. 
먼저 VM 명령어 창으로 다시 가셔서, CTRL+C를 눌러, 실행되고 있는 nginx 컨테이너를 종료 시킵니다.

그런 후에, 아래 명령어를 하나씩 실행해 줍니다. 
아직 실행 명령어가 어떤 의미인지는 잘 모르셔도 됩니다. (복붙하세욧! ㅎㅎ)

`docker run --name example1 -v /vagrant/example1/:/usr/share/nginx/html:ro -p 1001:80 -d nginx`   

`docker run --name example2 -v /vagrant/example2/:/usr/share/nginx/html:ro -p 1002:80 -d nginx`     

`docker run --name example3 -v /vagrant/example3/:/usr/share/nginx/html:ro -p 1003:80 -d nginx`     

### 4.7 확인하기

인터넷 브라우저 창에서 아래 주소를 입력하여 페이지가 잘 뜨는지를 확인합니다.  
`http://localhost:1234`   
`http://localhost:2345`  
`http://localhost:3456`  

뭔가 그럴싸해 보이죠? 실무에서 동작하는 수많은 컨테이너 어플리케이션은 이런 형태일 것입니다.

수고하셨습니다. 다음 실습 시간에 또 만나요.
