#!/bin/bash

curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh
sudo usermod -aG docker vagrant
sudo systemctl start docker
sudo systemctl enable docker

yum install -y xorg-x11-xauth firefox
sudo setsebool -P mozilla_read_content 1
sudo setsebool mozilla_plugin_can_network_connect=1