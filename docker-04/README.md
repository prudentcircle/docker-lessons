04. 실습 네 번째 시간 - 도커 이미지
## 1. 인사말
Docker 실습 네 번째 시간입니다. 이번에는 도커 이미지에 대한 실습을 해 보겠습니다. Dockerfile을 이용하여 우리가 원하는 맞춤형 도커 이미지를 만들어 볼 겁니다.

## 2.실습내용
첫 번째 실습 시간에 띄워보았던 화려한 nginx 웹페이지를 기억하시나요? 이번 시간엔 그 이미지를 직접 만들어 보는 시간을 가질까 합니다. 이번 실습엔 Xming 혹은 Xquartz가 필요합니다. 혹시나 설치하지 않으신 분은 여러분의 윈도우즈/맥 PC에 해당 프로그램를 꼭 설치해 놓으세요.

### 2.1 nginx Dockerfile 살펴보기
Official nginx 도커 이미지의  Dockerfile을 살펴보면 Dockerfile의 구조를 알 수 있습니다. 자세한 설명은 설명시간에 드렸죠?

https://github.com/nginxinc/docker-nginx/blob/f603bb3632ea6df0bc9da2179c18eb322c286298/mainline/stretch/Dockerfile

### 2.2 우리만의 nginx Dockerfile 만들기
우리만의 도커 이미지를 생성해 보겠습니다. 
/vagrant 디렉토리 안에 Dockerfile을 생성합니다. 대소문자는 꼭 지켜주세요.  
`vi /vagrant/Dockerfile`

Dockerfile 안에는 아래 내용을 넣어줍니다.  간단히 설명하면, example01 안에 있는 내용을 컨테이너 내의 /usr/share/nginx/html 디렉토리 안으로 복사하라는 내용입니다.
/usr/share/nginx/html 디렉토리는 제가 마음대로 정한게 아니라, nginx:latest 이미지를 만드신 분들이 그렇게 하라고 하셔서 그렇습니다. 
```
FROM nginx:latest
COPY ./example01/ /usr/share/nginx/html
```

### 2.3 도커 이미지 만들기
/vagrant 디렉토리로 이동합니다.
`cd /vagrant`
 도커 이미지를 생성합니다. 맨 마지막에 쩜은 잊지 말고 넣어주세요.
`docker build -t mynginx .`

### 2.4 우리 이미지로 컨테이너 생성하기
우리가 만든 이미지로 컨테이너를 실행시켜 봅니다.  
`docker run -d --name mynginx1 -p 80:80 mynginx`  
이제 우리 이미지를 확인해 볼 시간입니다.  

`firefox` 를 입력해서 여러분의 윈도우즈/맥에 firefox를 띄웁니다.

주소창에 `http://localhost:80`을 입력해서 페이지가 잘 뜨는지 확인해 봅니다.

### 2.5 두 번째 이미지 만들어 보기
지난 시간에, nginx 이미지 안에는 기본적으로 vim이 설치 되어있지가 않아서 우리가 수동으로 설치를 해 주었던것을 기억하시나요? 이번에는 vim이 기본으로 설치되어있는 nginx 컨테이너를 생성해 보겠습니다.

아래 내용으로 Dockerfile을 생성합니다.
근데 어쩌죠, 하나의 디렉토리 안에 두 개의 dockerfile은 생성이 힘들겠죠. 이름이 달라야 합니다.

그래서 dockerfile-nginx 라는 파일을 /vagrant에 생성합니다.    
`vi /vagrant/dockerfile-nginx`
```
FROM nginx:latest
RUN apt-get update && apt-get install vim
COPY index-hi.html /usr/share/nginx/html/index.html
```

/vagrant 디렉토리에서 이미지를 빌드 하는 아래 명령어를 실행합니다.  
`docker build -t nginx-vim -f dockerfile-nginx .`

새로 만든 이미지를 사용하여 도커 인스턴스를 실행합니다.  
`docker run -d --name nginxvim1 -p 1200:80 nginx-vim`

내용을 확인합니다.  
`firefox`  
아래 주소로 확인합니다. (Xming이나 Xquartz가 필요합니다.)  
`http://localhost:1200`  

이제는 내용을 수정해 볼까요. 컨테이너 안으로 들어갑니다.  
`docker exec -it nginxvim1 /bin/bash`

vim이 이제는 바로 실행됩니다. 아래 명령어를 실행해 봅시다.  
`vim /usr/share/nginx/html/index.html`  
내용을 마음에 드는대로 고쳐보고 다시 확인해 보세요.


### 2.6 세 번째 이미지 만들어 보기

우리는 dockerfile-jar 이라는 이름으로 Dockerfile을 생성합니다.
```
FROM openjdk:8-jdk-alpine
COPY dockerlab.jar dockerlab.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/dockerlab.jar"]
```

`docker build -t springboot:1.0 -f dockerfile-jar .`을 입력하여 이미지를 생성합니다.

이미지가 생성되면 실행을 해 봅니다.
`docker container run -d --name spring01 --publish 1300:80 springboot:1.0`

이제 확인을 해 봅니다. (Xming이나 Xquartz가 필요합니다.)  
`firefox`  
아래 주소로 접속해 보세요.  
`http://localhost:1300`  

404 에러메시지가 뜨면 제대로 동작 하는것입니다.
에러메시지가 제대로 동작한다는 증거라니, 조금 웃기기는 하죠 ^^;;