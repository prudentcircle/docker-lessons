# 03. 실습 세 번째 시간 - 네트워크
## 1. 인사말
Docker 실습 세 번째 시간입니다. 이번에는 Docker의 네트워크에 대해서 이런저런 실습을 해 보도록 하겠습니다. 

## 2.실습내용
이번 실습에서는 Xming 이나 Xquartz가 필요하지는 않습니다. 그냥 명령어만 입력할 수 있는 환경이면 충분합니다.

### 2.1 포트 설정하기
가장 기본적이면서도 중요한 기능이죠. 우리는 이미 이 기능을 사용해 본적이 있습니다.  `--publish` 옵션을 사용하면 우리는 특정 포트를 열어줄수가 있습니다.
`--publish 100:80`  를 예로 들어봅시다.  
하나만 기억하세요. 왼쪽이 바깥! 오른쪽이 안쪽! 바깥에서 100번 포트로 접속하면 컨테이너 내부의 80번으로 접속하게 하라는 의미입니다.

컨테이너의 포트에 대한 정보만 확인하고 싶다면 아래 명령어를 활용하세요.   
`docker container port <container>`

### 2.2 네트워크 분리

A부서가 사용하는 Nginx 컨테이너와 B부서가 사용하는 컨테이너는 각각 다른 네트워크에 분리되어서 동작해야 할 수도 있습니다. 그래서 네트워크를 나눠보겠습니다. 

### 2.3 네트워크 확인
현재 docker에서 사용하는 네트워크가 어떤것이 있는지를 확인해 봅니다.   
`docker network ls`  

bridge, host가 있는것이 보입니다.  기본 bridge 네트워크를 사용해도 되지만,
기본 bridge 네트워크는 DNS 기능을 제공하지 않습니다.

### 2.4 네트워크 생성
네트워크를 생성해 봅시다. 우리는 vita500 이라는 이름의 네트워크를 새로 생성합니다.  
`docker network create vita500`  
네트워크가 잘 생성되었는지 확인합니다.  
`docker network ls`

### 2.5 컨테이너 생성
busybox 컨테이너를 여러개 생성합니다. 네트워크에 대한 설정을 하지 않습니다. 기본 bridge 네트워크에 생성하고 싶으니까요.  
`docker container run -d --name busybox1 --restart=always busybox sleep 100000`  
`docker container run -d --name busybox2 --restart=always busybox sleep 100000`  
`docker container run -d --name busybox3 --restart=always busybox sleep 100000`  

busybox1 컨테이너에 들어갑니다.  
`docker container exec -it busybox1 /bin/sh`  
여기서 busybox2와 busybox3으로 ping 을 시도해 봅니다.
잘 안되죠?   
`exit` 을 입력하여 빠져나옵니다.

### 2.6 네트워크 지정하여 생성
이제는 vita500 이라는 네트워크 안에 busybox 4,5,6번을 생성합니다.  
`docker container run -d --name busybox4 --restart=always --network vita500 busybox sleep 100000`
`docker container run -d --name busybox5 --restart=always --network vita500 busybox sleep 100000`
`docker container run -d --name busybox6 --restart=always --network vita500 busybox sleep 100000`

busybox6번 컨테이너에 들어갑니다.   
`docker container exec -it busybox6 /bin/sh`  
busybox4,5번으로 ping을 시원하게 때려봅니다.  
`ping busybox4`  
`ping busybox5`  
잘 가죠? DNS가 자동으로 지원되는것을 확인할 수 있습니다.  
`exit`를 입력해 빠져나옵니다.

### 2.7 네트워크에 추가해주기
busybox1,2,3 번을 vita500 네트워크에 추가해 보겠습니다.  

`docker network connect vita500 busybox1`  
`docker network connect vita500 busybox2`  
`docker network connect vita500 busybox3`  

`docker network inspect vita500` 명령어를 사용하여 현재 어떤 컨테이너가 네트워크에 속해있는지 확인해 봅니다.
`docker network inspect bridge` 명령어를 사용하여 현재 어떤 컨테이너가 네트워크에 속해 있는지 확인해 봅니다.

어라? 1,2,3 번이 양다리를 걸치고 있는게 보이죠?  
busybox1에 접근해 봅니다.  
`docker container exec -it busybox1 /bin/sh`  
NIC이 몇개인지를 확인해 봅니다.  
`ip addr show`  
2개죠? busybox6번 컨테이너로 ping을 때려봅니다.  
`ping busybox6`  
잘 가죠?  

이번엔, busybox6번 컨테이너에 접근합니다.  
`docker container exec -it busybox6 /bin/sh`  
여기에는 NIC이 몇 개일까요? 한개죠?  
`ip addr show`  
busybox1번으로 이제 ping이 잘 가는것을 확인해 봅니다.  
`ping busybox1`  

### 2.8 네트워크 제거해주기
busybox1가 양다리를 걸치고 있는것이 마음에 들지 않습니다. 우리는 busybox1을 bridge 네트워크에서 빼버리겠습니다.  

`docker network disconnect bridge busybox1`  

busybox1번에 접속해 봅니다.  
`docker container exec -it busybox6 /bin/sh`  
NIC이 몇 개인지 확인합니다.  
`ip addr show`  
이젠 몇 개죠?  

### 2.9 신기한 기능 체험해 보기
DNS 를 이용한 round-robin routing을 해 보신적이 있으신가요?
docker에서는 이것이 가능합니다.  

하아.. 시간이 없어서 데모를 못 만들겠네요. 다음기회에 올리겠습니다.   

`--net-alias` 옵션을 사용해서 하는 건데요. 다음 기회로 미루도록 하겠습니다. 
이 이외에도 기본적인  bridge 이외에 복잡하고 멋있는 기술을 사용하고 싶다면 `docker network create --driver` 명령어를 사용할 수 있습니다.   

## 3. 요약
오늘 배운 명령어를 요약해 봅시다.

- `docker network ls`
- `docker network create vita500`
- `docker container run -d --name busybox4 --restart=always --network vita500 busybox sleep 100000`
- `docker network connect vita500 busybox1`
- `docker network disconnect bridge busybox1`
- `docker network inspect vita500`

