import socket
from flask import Flask


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(('8.8.8.8', 1))  # connect() for UDP doesn't send packets
local_ip_address = s.getsockname()[0]

app = Flask(__name__)

@app.route('/')
def hello_world():
    return local_ip_address